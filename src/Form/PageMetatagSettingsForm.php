<?php

namespace Drupal\page_metatag\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Page metatag settings for this site.
 */
class PageMetatagSettingsForm extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs an PageMetatagSettingsForm object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(LanguageManagerInterface $language_manager = NULL, ModuleHandlerInterface $module_handler) {
    if ($language_manager) {
      $this->languageManager = $language_manager;
    }
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $languageServices = NULL;
    if ($container->has('language_manager')) {
      $languageServices = $container->get('language_manager');
    }
    return new static(
      $languageServices,
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'page_metatag_settings_form';
  }

  /**
   *{@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'page_metatag.settings',
    ];
  }

  /**
   *{@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('page_metatag.settings');
    $form['desc'] = [
      '#type' => 'markup',
      '#markup' => $this->t('
        <b>Enabled:</b> Rule will work only if checkbox is checked.<br>
        <b>Pages:</b> Enter one path per line. The "*" character is a wildcard. Example paths are "/node/1" for an individual piece of content or "/node/*" for every piece of content. "@front" is the front page.<br>'),
    ];
    // Headers for table.
    $header = [
      ['data' => $this->t('Enabled'), 'width' => 50],
      ['data' => $this->t('Pages'), 'width' => 300],
      ['data' => $this->t('Page Title/Keywords'), 'width' => 400],
      ['data' => $this->t('Description'), 'width' => 400],
    ];
    if ($this->languageManager->isMultilingual() || $this->moduleHandler->moduleExists('language')) {
      $header[] = $this->t('Language');
    }
    array_push($header, $this->t('Operation'), $this->t('Weight'));

    // Multi value table form.
    $form['page_metatag_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('There are no items yet. Add an item.', []),
      '#prefix' => '<div id="spt-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'page_metatag_table-order-weight',
        ],
      ],
    ];

    // Set table values on Add/Remove or on page load.
    $page_metatag_table = $form_state->get('page_metatag_table');
    if (empty($page_metatag_table)) {
      // Set data from configuration on page load.
      // Set empty element if no configurations are set.
      if (NULL !== $config->get('page_metatag_table')) {
        $page_metatag_table = $config->get('page_metatag_table');
        $form_state->set('page_metatag_table', $page_metatag_table);
      }
      else {
        $page_metatag_table = [''];
        $form_state->set('page_metatag_table', $page_metatag_table);
      }
    }

    // Create row for table.
    foreach ($page_metatag_table as $i => $value) {
      $form['page_metatag_table'][$i]['#attributes']['class'][] = 'draggable';
      $form['page_metatag_table'][$i]['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Status'),
        '#title_display' => 'invisible',
        '#default_value' => $value['status'] ?? '',
      ];
      $form['page_metatag_table'][$i]['pages'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Pages'),
        '#title_display' => 'invisible',
        '#required' => TRUE,
        '#cols' => '5',
        '#rows' => '5',
        '#default_value' => $value['pages'] ?? [],
      ];
      $metadata = [
        'page_title' => [
          '#type' => 'textfield',
          '#title' => $this->t('Page Title'),
          '#title_display' => 'invisible',
          '#required' => TRUE,
          '#size' => 10,
          '#maxlength' => 60,
          '#attributes' => ['placeholder' => 'Title'],
          '#default_value' => $value['title']['page_title'] ?? [],
        ],
        'page_keywords' => [
          '#type' => 'textfield',
          '#title' => $this->t('Page Keywords'),
          '#title_display' => 'invisible',
          '#size' => 10,
          '#maxlength' => 255,
          '#description' => t('Keywords separate by comma.'),
          '#attributes' => ['placeholder' => 'Keywords'],
          '#default_value' => $value['title']['page_keywords'] ?? [],
        ],
      ];
      $form['page_metatag_table'][$i]['title'] = $metadata;
      $form['page_metatag_table'][$i]['description'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Description'),
        '#title_display' => 'invisible',
        '#required' => TRUE,
        '#cols' => '5',
        '#rows' => '5',
        '#maxlength' => 160,
        '#default_value' => $value['description'] ?? [],
      ];
      // Add Language if site is multilingual.
      if ($this->languageManager->isMultilingual() || $this->moduleHandler->moduleExists('language')) {
        foreach ($this->languageManager->getLanguages() as $langkey => $langvalue) {
          $langNames[$langkey] = $langvalue->getName();
        }
        $form['page_metatag_table'][$i]['language'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Language'),
          '#title_display' => 'invisible',
          '#options' => $langNames,
          '#default_value' => $value['language'] ?? [],
        ];
      }
      $form['page_metatag_table'][$i]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => "remove-" . $i,
        '#submit' => ['::removeElement'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::removeCallback',
          'wrapper' => 'spt-fieldset-wrapper',
        ],
        '#index_position' => $i,
      ];
      // TableDrag: Weight column element.
      $form['page_metatag_table'][$i]['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $value['weight'] ?? [],
        '#attributes' => ['class' => ['page_metatag_table-order-weight']],
      ];
    }
    $form['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'spt-fieldset-wrapper',
      ],
    ];
    $form['token_key']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['current-page', 'site', 'node'],
      '#show_restricted' => TRUE,
      //'#global_types' => FALSE,
      '#weight' => 1,
    ];

    $form_state->setCached(FALSE);

    return parent::buildForm($form, $form_state);
  }

  /**
   * A addMore callback method.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The rendered page metatag table.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['page_metatag_table'];
  }

  /**
   * A addOne method.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $page_metatag_table = $form_state->get('page_metatag_table');
    array_push($page_metatag_table, "");
    $form_state->set('page_metatag_table', $page_metatag_table);
    $form_state->setRebuild();
  }

  /**
   * A remove callback method.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The rendered page metatag table.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    return $form['page_metatag_table'];
  }

  /**
   * A removeElement method.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function removeElement(array &$form, FormStateInterface $form_state) {
    // Get table.
    $page_metatag_table = $form_state->get('page_metatag_table');
    // Get element to remove.
    $remove = key($form_state->getValue('page_metatag_table'));
    // Remove element.
    unset($page_metatag_table[$remove]);
    // Set an empty element if no elements are left.
    if (empty($page_metatag_table)) {
      array_push($page_metatag_table, "");
    }
    $form_state->set('page_metatag_table', $page_metatag_table);
    $form_state->setRebuild();
  }

  /**
   * A submitForm method.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('page_metatag.settings')->set('page_metatag_table', $form_state->getValue('page_metatag_table'))->save();
    parent::submitForm($form, $form_state);
  }

}
