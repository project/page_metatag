CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## CONTENTS OF THIS FILE
------------

This module will add field formatter on every field type and
profile type in manage display section where user can change
the field format.

## INTRODUCTION
------------

 -No any special requirements.

## REQUIREMENTS
------------

Install via /admin/modules
drush en html_script_formatter -y
composer require drupal/html_script_formatter

## INSTALLATION
-----------

Current maintainers:
 Radheshyam Kumawat - https://www.drupal.org/u/radheymkumar
 Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
