<?php

/**
 * @file
 * Contains page_metatag.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;

/**
 * Implements hook_help().
 */
function page_metatag_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the page_metatag module.
    case 'help.page.page_metatag':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Allow meta tags for all page and entities.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_page_attachments_alter().
 */
function page_metatag_page_attachments_alter(array &$page) {
  $page_metatag_config = \Drupal::config('page_metatag.settings')->get('page_metatag_table');
  if (!$page_metatag_config) {
    return FALSE;
  }

  $current_path = \Drupal::service('path.current')->getPath();
  $pathMatcher = \Drupal::service('path.matcher');
  $pathAliasManager = \Drupal::service('path_alias.manager');
  $languageManager = \Drupal::languageManager();
  $moduleHandler = \Drupal::service('module_handler');
  foreach ($page_metatag_config as $key => $value) {
    if ($value['status'] == 1) {
      $pages = $value['pages'];
      // If match conditions.
      $pathAlias = $pathAliasManager->getAliasByPath($current_path);
      $condition = ($pathMatcher->matchPath($current_path, $value['pages']) ||
      $pathMatcher->matchPath($pathAlias, $value['pages']));
      // Check if site is multilingual.

      $languageModuleExists = $moduleHandler->moduleExists('language');
      if ($languageManager->isMultilingual() || $languageModuleExists) {
        // Check condition for language.
        $langId = $languageManager->getCurrentLanguage()->getId();
        $condition = ($condition && (!array_filter($value["language"]) ||
        !empty(array_intersect($value["language"], [$langId]))));
      }
      // Check if all conditions match.
      if ($condition) {
        // Load entity type and current entity.
        $object = [];
        foreach (\Drupal::routeMatch()->getParameters() as $key => $param) {
          if ($param instanceof EntityInterface) {
            $object[] = $param;
            $entity = $key;
          }
        }
        $id = $object[0]->id();
        $html_head = [
          'description' => [
            '#type' => 'html_tag',
            '#tag' => 'meta',
            '#attributes' => [
              'name' => 'description',
              'content' => page_metatag_get_metatag_content($value['description'], $entity, $id),
            ],
          ],
          'keywords' => [
            '#type' => 'html_tag',
            '#tag' => 'meta',
            '#attributes' => [
              'name' => 'keywords',
              'content' => page_metatag_get_metatag_content($value['title']['page_keywords'], $entity, $id),
            ],
          ],
          'title' => [
            '#type' => 'html_tag',
            '#tag' => 'title',
            '#value' => page_metatag_get_metatag_content($value['title']['page_title'], $entity, $id),
            '#weight' => -11,
          ],
        ];

        foreach ($html_head as $key => $data) {
          $page['#attached']['html_head'][] = [$data, $key];
        }
      }
    }
  }
}

/**
 * Page metatagToken replace.
 */
function page_metatag_get_metatag_content($value, $entity, $id) {
  $token_service = \Drupal::token();
  $entityObject = \Drupal::entityTypeManager()->getStorage($entity)->load($id);
  $replacement = $token_service->replace($value, [$entity => $entityObject]);
  $sanitize_value = page_metatag_sanitize_text($replacement);
  return $sanitize_value;
}

/**
 * Page metatag Sanitize input.
 */
function page_metatag_sanitize_text($string) {
  $string = trim($string);
  $string = strip_tags($string);
  $string = Html::escape($string);
  $string = Xss::filter($string);
  $string = substr($string, 0, 160);
  return $string;
}
